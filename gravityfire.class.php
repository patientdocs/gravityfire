<?php
class GravityFire {
	protected $use_bootstrap = true;

	protected $meta_fields = array(
		"universal_leadid" => array(
			'label' => "Lead ID",
			'is_numeric' => false,
			'is_default_column' => true,
			'bluefire_name' => 'leadid'
		),
		"usertime" => array(
			'label' => "User Time",
			'is_numeric' => false,
			'is_default_column' => false,
		),
		"optin_cert" => array(
			'label' => "Optin Cert",
			'is_numeric' => false,
			'is_default_column' => false,
		),
		"___pageid___" => array(
			'label' => "Page ID",
			'is_numeric' => false,
			'is_default_column' => false,
		),
		"platform" => array(
			'label' => "Platform",
			'is_numeric' => false,
			'is_default_column' => false,
		),
		"REMOTE_ADDR" => array(
			'label' => "Remote IP Address",
			'is_number' => false,
			'is_default_column' => false,
			'bluefire_name' => 'ipaddress'
		),
		"HTTP_USER_AGENT" => array(
			'label' => "User Agent",
			'is_number' => false,
			'is_default_column' => false,
			'bluefire_name' => 'useragent'
		),
		"HTTP_REFERER" => array(
			'label' => "URL of Request",
			'is_number' => false,
			'is_default_column' => false,
			'bluefire_name' => 'requesturl'
		),
	);
	public function add_form_meta( $entry_meta, $form_id ) {
		//add local function here
		foreach( $this->meta_fields as $k=>$m ) {
			$this->meta_fields[$k]['update_entry_meta_callback'] = array($this, 'update_form_meta_from_request');
			$entry_meta[$k] = $this->meta_fields[$k];
		}
		return $entry_meta;
	}

	public function update_form_meta_from_request( $key, $lead, $form ) {
		$value = ( isset( $_REQUEST[$key]) ? $_REQUEST[$key] : ( isset($_SERVER[$key]) ? $_SERVER[$key] : '' ) );
		//echo "
		//<br /> ASKING FOR: ".$key." GETTING: ".$value;
		return $value;
	}

	public function use_bootstrap( $use = false ) { $this->use_bootstrap = $use; }
	public function __construct() {}
	public function activate() { return true; }
	public function deactivate() { return true;	}

	public function enqueue_styles() {
		//if ( !wp_script_is('bootstrap') && $this->use_bootstrap )
			// 	    wp_enqueue_style( 'bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css');
			//         wp_enqueue_style('gf_bootstrap_css', plugins_url('/css/gforms_styles.css',__FILE__ ), [], '2' );
	}

	public function enqueue_scripts() {
		//         if ( !wp_script_is('jquery') )
			//             wp_enqueue_script( 'jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js' );
	}

	/**
	 * Given a form entry, build an array of entry data keyed with its field IDs.
	 *
	 * The resulting array will include any value from $entry on a $form field with an assigned fieldID
	 * property. Complex fields are handled as long as the field IDs are passed as a comma-separated
	 * list _and_ we have enough IDs for each non-hidden input within a field.
	 *
	 * For example, if $form has a GF_Field_Name field containing a first and last name, but we only
	 * provide a single field ID (e.g. "name"), only the first name would be saved. Instead, we want to
	 * be sure we're using field IDs like "firstname, lastname" to ensure that all data gets mapped.
	 *
	 * @param array $entry The Gravity Forms entry object.
	 * @param array $form  The Gravity Forms form object.
	 * @return array An array of entry values from fields with IDs attached.
	 */
	function get_mapped_fields( $entry, $form ) {
		$mapping = array();
		foreach ( $form['fields'] as $field ) {
			if ( ! isset( $field['mapperID'] ) || ! $field['mapperID'] ) {
				continue;
			}
			// Explode field IDs.
			$field_ids = array_map( 'trim', explode( ',', $field['mapperID'] ) );

			// We have a complex field, with multiple inputs.
			if ( ! empty( $field['inputs'] ) ) {
				foreach ( $field['inputs'] as $input ) {
					if ( isset( $input['isHidden'] ) && $input['isHidden'] ) {
						continue;
					}
					$field_id = array_shift( $field_ids );
					// If $field_id is empty, don't map this input.
					if ( ! $field_id ) {
						continue;
					}
					// Finally, map this value based on the $field_id and $input['id'].
					$mapping[ $field_id ] = $entry[ $input['id'] ];
				}
			} else {
				$mapping[ $field_ids[0] ] = ( $field['type'] == 'html' ? $field['content'] : $entry[ $field['id'] ] );
			}
		}

		return $mapping;
	}

	/**
	 * Print custom scripting for the "Mapper ID" property.
	 */
	function editor_script() {
		include( 'js/mapper_id_editor.js');
	}

	/**
	 * Render the "Mapper ID" property for Gravity Form fields under the "Advanced" tab.
	 *
	 * @param int $position The current property position.
	 */
	public function render_mapper_id_setting( $position ) {
		if ( 50 !== $position ) {
			return;
		}
		include('views/admin_mapper_id_field.html');
	}

	public function after_submit( $entry, $form ) {
		//submit form to lead source after renaming fields
		//use curl
		// 	    echo "<pre>";
		// 	    print_r( $form );
		// 	    print_r( $entry );
		// 	    echo "</pre>";
		// 	    die();
		$mapped_fields = $this->get_mapped_fields( $entry, $form );
		require_once 'bluefireleads.class.php';
		$fire = new bluefireleads();
		$lead = new bluefirelead();

		//get mapper id fields
		foreach( $mapped_fields as $k=>$f ) {
			if ( $k == 'firetoken' ) $fire->setKey( $f );
			else $lead->$k = $f;
		}
		//get meta fields
		foreach( $this->meta_fields as $k => $m ) {
			if ( property_exists($lead, $k ) ) $lead->$k = $entry[$k];
			if ( key_exists('bluefire_name', $m) && property_exists($lead, $m['bluefire_name'] )  ) $lead->$m['bluefire_name'] = $entry[$k];
		}

		$result = $fire->submit( $lead );
		// 	    gform_update_meta( $entry['id'], 'leadid', $_REQUEST['universal_leadid'] );
		// 	    print_r( $entry );
	}

	public function form_render( $form ) {
		$lead_id = new GF_Field_HTML();
		$lead_id['content'] = file_get_contents( __DIR__.'/views/form_lead_id_field.html');
		array_push( $form['fields'], $lead_id );
		// 		print_r( $form );
		// 		die();
		$props = array(
			'id' => 'platform',
			'label' => 'platform',
			'defaultValue' => 'default',
			'allowsPrepopulate' => true,
			'type' => 'hidden',
			'mapperID' => 'platform',
			'inputName' => 'platform'
		);
		$field = GF_Fields::create( $props );
		array_push( $form['fields'], $field );
		return $form;
	}

	public function form_field_container( $field_container, $field, $form, $css_class, $style, $field_content ) {
		//add bootstrap classes
		$id = $field->id;
		$field_id = is_admin() || empty( $form ) ? "field_{$id}" : 'field_' . $form['id'] . "_$id";
		return '<li id="' . $field_id . '" class="' . $css_class . ' form-group">{FIELD_CONTENT}</li>';
	}

	public function form_init_scripts( $form ) {
		$lead_script = file_get_contents( __DIR__."/js/form_init.js");
		$zip_script = file_get_contents( __DIR__."/js/zip-autofill.js");
		// 	    $mapper_script = file_get_contents( __DIR__."/js/gform_confirmation_loaded.js");
		// 	    GFFormDisplay::add_init_script($form['id'], 'MapperScript', GFFormDisplay::ON_PAGE_RENDER, $mapper_script );
		GFFormDisplay::add_init_script($form['id'], 'LeadiDscript', GFFormDisplay::ON_PAGE_RENDER, $lead_script);
		GFFormDisplay::add_init_script($form['id'], 'ZipScript', GFFormDisplay::ON_PAGE_RENDER, $zip_script);
	}

	public function init() {
		//actions
		add_action( 'gform_editor_js', [$this, 'editor_script'] );
		add_action( 'gform_field_advanced_settings', [$this, 'render_mapper_id_setting'] );
		add_action( 'gform_after_submission', [$this,'after_submit'], 10, 2 );
		add_action( 'gform_pre_render', [$this,'form_render'] );
		add_action( 'gform_entry_meta', [$this, 'add_form_meta'], 10, 2 );
		add_action( 'gform_register_init_scripts', [$this, 'form_init_scripts'] );

		add_action( 'wp_enqueue_scripts', [$this, 'enqueue_scripts'] );
		add_action( 'wp_enqueue_scripts', [$this, 'enqueue_styles'] );
		//filters
		add_filter( 'gform_enable_field_label_visibility_settings', '__return_true' );
		//  	    add_filter( 'gform_field_container', [$this, 'form_field_container'], 10, 6 );

	}

}
