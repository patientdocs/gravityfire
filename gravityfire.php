<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://BlueFireLeads.com
 * @since             1.0.0
 * @package           GravityFire
 *
 * @wordpress-plugin
 * Plugin Name:       Gravity Fire
 * Plugin URI:        http://BlueFireLeads.com
 * Description:       Enhances Gravity forms with functionality needed to process forms for BlueFire
 * Version:           1.5.1
 * Author:            Parl Johnson
 * Author URI:        http://BlueFireLeads.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       blue-fire
 * Domain Path:
 */
define('FIRE_PLUGIN_REF', 'gravityfire/gravityfire.php');
define('FIRE_USE_BOOTSTRAP', false ); //placeholder for now until we can add a meta value in the database for it and make an admin screen
// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}
require_once 'gravityfire.class.php';
$fire = new GravityFire();
// if ( is_plugin_active( FIRE_PLUGIN_REF ) ) {
    $fire->use_bootstrap( FIRE_USE_BOOTSTRAP );
    $fire->init();
// }

register_activation_hook( __FILE__, array( $fire, 'activate') );
register_deactivation_hook( __FILE__, array( $fire, 'deactivate') );

?>
