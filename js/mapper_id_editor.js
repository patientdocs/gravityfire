<script type="text/javascript">
    // Add .field_id_setting onto the end of each field type's properties.
    jQuery.map(fieldSettings, function (el, i) {
      fieldSettings[i] += ', .mapper_id_setting';
    });

    // Populate field settings on initialization.
    jQuery(document).on('gform_load_field_settings', function(ev, field){
      jQuery(document.getElementById('field_mapper_id')).val(field.mapperID || '');
    });
</script>