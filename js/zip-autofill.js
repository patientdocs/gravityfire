$ = jQuery;
$(function() {

	var Autofiller = function() {

	};
	Autofiller.prototype = {
		init: function() {
			var self = this;
			self.geocoder = new google.maps.Geocoder();
			$('.zip input').each(function() {
				console.log( this );
				$(this).on('change keyup', self.autofill.bind(self) );
			});
		},

		autofill: function(event) {
			var zipc = $(event.currentTarget).val().replace(/_/g, '');
			//alert( zipc.replace(/_/g, '') );
			if ( zipc.length != 5 ) return true;
			console.log( 'running event', event );
			var el = $(event.currentTarget),
				val = el.val()
			if (val.length < 5) return;

			var zipInput = el,
				cityInput = el.parents('form').find('.city input'),
				stateInput = el.parents('form').find('.state input');
			this.geocoder.geocode({address: val}, function(results, status) {
				var result = results.filter(function(nextResult) {
					return nextResult.address_components.pop().short_name === 'US'; // filter out non-US results
				});
				result = result[0]; // assume it's the first US result
				if (!result) return;

				var city = result.address_components[1].short_name,
					state = result.address_components.pop().short_name;
				if (!city || !state || state.length !== 2) return; // The result isn't what's expected; stop execution.

				// Autofill!
				cityInput.val(city);
				stateInput.val(state);
				console.log( cityInput, stateInput );
			});
		}
	};

	var autofiller = new Autofiller();

	if (!$('script').filter(function() { return this.src.match(/google\.com\/maps\/api\/js/); }).length) {
		var script = document.createElement('script');
		$('head').append(script);

		script.onload = autofiller.init.bind(autofiller);
		script.src = 'https://maps.google.com/maps/api/js?key=AIzaSyCsy4AYbwbX5Doj8s-pNHfijo0KOSgFkMw';

		return;
	}

	autofiller.init();
});
